﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConcordanceGenerator.Helper
{
    public static class Helpers
    {
        private static readonly Encoding ContentEncoding = Encoding.UTF8;
        private const string REQUEST_CONTENT_TYPE = "text/html; charset=utf-8";
        private const string REQUEST_AGENT_TYPE = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)";
        private const string REQUEST_USERAGENT = "UserAgent";

        /// <summary>
        /// Get content of a given web url
        /// </summary>
        /// <param name="url">given url</param>
        /// <returns>content in text</returns>
        public static string GetWebResponse(string url)
        {
            var uri = new Uri(url);
            if (uri == null) return null;
            return GetWebResponse(uri);
        }

        /// <summary>
        /// Get content of a given web uri
        /// </summary>
        /// <param name="url">given uri</param>
        /// <returns>content in text</returns>
        public static string GetWebResponse(Uri uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.KeepAlive = true;
            request.ProtocolVersion = HttpVersion.Version10;
            request.ServicePoint.ConnectionLimit = 30;
            request.Headers.Add(REQUEST_USERAGENT, REQUEST_AGENT_TYPE);
            request.ContentType = REQUEST_CONTENT_TYPE;

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
            catch (WebException)
            {
                return null;
            }
        }

        /// <summary>
        /// to tell if each item in 2 arraies matches each other
        /// </summary>
        /// <typeparam name="T">genaric type</typeparam>
        /// <param name="expeted">expected array</param>
        /// <param name="actural">actural array</param>
        /// <returns>match or not</returns>
        public static bool ArrayEqual<T>(ICollection<T> expeted, ICollection<T> actural)
        {
            if (expeted == null)
            {
                if (actural != null) return false;
                else return true;
            }

            if (actural == null)
            {
                if (expeted != null) return false;
                else return true;
            }

            if (expeted.Count != actural.Count) return false;

            var result = true;
            for (var i = 0; i < expeted.Count; i++)
            {
                result = result && expeted.ElementAt(i).Equals(actural.ElementAt(i));
                if (!result) return false;
            }

            return true;
        }
    }
}
