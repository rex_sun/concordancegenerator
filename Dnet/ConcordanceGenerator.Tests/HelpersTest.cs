﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConcordanceGenerator.Helper;

namespace ConcordanceGenerator.Tests
{
    [TestClass]
    public class HelpersTest
    {
        [TestMethod]
        public void GetWebResponseTest_CanRead()
        {
            var actual = Helpers.GetWebResponse("http://www.google.com");
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void GetWebResponseTest_ExactlyRead()
        {
            var expected = SampleText.Hamlet;
            var actual = Helpers.GetWebResponse("http://www.gutenberg.org/cache/epub/1524/pg1524.txt");
            Assert.AreEqual(expected, actual);
        }
    }
}
