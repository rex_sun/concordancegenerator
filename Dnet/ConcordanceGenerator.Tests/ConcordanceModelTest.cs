﻿using ConcordanceGenerator.Helper;
using ConcordanceGenerator.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ConcordanceGenerator.Tests
{
    [TestClass]
    public class ConcordanceModelTest
    {
        [TestMethod]
        public void GenerateConcordance_OneSentence()
        {
            var model = new ConcordanceModel(SampleText.OneSentence_1);
            model.GenerateConcordance();
            var expectedOccurs = 1;
            var acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("we");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("worl");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            var expectedOccursIndex = new List<int> { 5 };
            var acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 0 };
            acturalOccursIndex = model.GetWordOccuredSentenceIndexes("world");
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.OneSentence_2);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 5, 26 };
            acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.OneSentence_3);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("we");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 12, 44 };
            acturalOccursIndex = model.GetWordOccursInSentence("we", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 5, 35 };
            acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.OneSentence_4);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 1;
            acturalOccurs = model.GetWordOccurs("saying");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("this");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("expect");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 5, 38 };
            acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 12 };
            acturalOccursIndex = model.GetWordOccursInSentence("saying", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 21 };
            acturalOccursIndex = model.GetWordOccursInSentence("this", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 59 };
            acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.OneSentence_5);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 1;
            acturalOccurs = model.GetWordOccurs("saying");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("this");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("expect");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 5, 61 };
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("better");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("'bet8*ter");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 12 };
            acturalOccursIndex = model.GetWordOccursInSentence("saying", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 42 };
            acturalOccursIndex = model.GetWordOccursInSentence("this", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 87 };
            acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 0 };
            acturalOccursIndex = model.GetWordOccuredSentenceIndexes("world");
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        }

        [TestMethod]
        public void GenerateConcordance_TwoSentences()
        {
            var model = new ConcordanceModel(SampleText.TwoSentences_1);
            model.GenerateConcordance();
            var expectedOccurs = 2;
            var acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("we");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            var expectedOccursIndex = new List<int> { 5 };
            var acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 10 };
            acturalOccursIndex = model.GetWordOccursInSentence("world", 1);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 0, 1 };
            acturalOccursIndex = model.GetWordOccuredSentenceIndexes("world");
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.TwoSentences_2);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("have");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 15 };
            acturalOccursIndex = model.GetWordOccursInSentence("have", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 11 };
            acturalOccursIndex = model.GetWordOccursInSentence("have", 1);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.TwoSentences_3);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("we");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 1;
            acturalOccurs = model.GetWordOccurs("it");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 12, 44 };
            acturalOccursIndex = model.GetWordOccursInSentence("we", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 10 };
            acturalOccursIndex = model.GetWordOccursInSentence("it", 1);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.TwoSentences_4);
            model.GenerateConcordance();
            expectedOccurs = 3;
            acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("a");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("expect");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 5, 38 };
            acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 27 };
            acturalOccursIndex = model.GetWordOccursInSentence("world", 1);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 59 };
            acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 11 };
            acturalOccursIndex = model.GetWordOccursInSentence("expect", 1);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));

            model = new ConcordanceModel(SampleText.TwoSentences_5);
            model.GenerateConcordance();
            expectedOccurs = 2;
            acturalOccurs = model.GetWordOccurs("world");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccurs = model.GetWordOccurs("saying");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccursIndex = new List<int> { 5, 61 };
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("better");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("'bet8*ter");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 12 };
            acturalOccursIndex = model.GetWordOccursInSentence("saying", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 24 };
            acturalOccursIndex = model.GetWordOccursInSentence("saying", 1);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 87 };
            acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 0, 1 };
            acturalOccursIndex = model.GetWordOccuredSentenceIndexes("saying");
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        }

        [TestMethod]
        public void GenerateConcordance_Hamlet()
        {
            var model = new ConcordanceModel(SampleText.Hamlet);
            model.GenerateConcordance();
            var expectedOccurs = 298;
            var acturalOccurs = model.GetWordOccurs("his");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 338;
            acturalOccurs = model.GetWordOccurs("this");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 118;
            acturalOccurs = model.GetWordOccurs("Hamlet");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            expectedOccurs = 0;
            acturalOccurs = model.GetWordOccurs("Hamleet");
            Assert.AreEqual(expectedOccurs, acturalOccurs);
            var expectedOccursIndex = new List<int> { 0 };
            var acturalOccursIndex = model.GetWordOccursInSentence("Project", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 119, 189 };
            acturalOccursIndex = model.GetWordOccursInSentence("Copyright", 0);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
            expectedOccursIndex = new List<int> { 0 };
            acturalOccursIndex = model.GetWordOccursInSentence("Please", 2);
            Assert.IsTrue(Helpers.ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        }
    }
}
