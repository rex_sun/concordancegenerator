﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConcordanceGenerator.Services
{
    /// <summary>
    /// Concordance model
    /// </summary>
    public class ConcordanceModel
    {
        /// <summary>
        /// . ? ! are the scentence spliter
        /// </summary>
        private static readonly Regex regSentenceSpliter = new Regex("[.?!]", RegexOptions.Compiled | RegexOptions.Multiline);

        /// <summary>
        /// all others except for the english characters will be considered as word spliters
        /// </summary>
        private static readonly Regex regWordSpliter = new Regex("[^a-zA-Z]", RegexOptions.Compiled | RegexOptions.Multiline);

        /// <summary>
        /// all english characters is valid word character
        /// </summary>
        private static readonly Regex regWordCharacter = new Regex("[a-zA-Z]+", RegexOptions.Compiled | RegexOptions.Multiline);

        /// <summary>
        /// initialize a ConcordanceModel class by the target text
        /// </summary>
        /// <param name="text">the text you want to generate concordance</param>
        public ConcordanceModel(string text)
        {
            this.OriginalText = text;
            this.FillSentenceEnds();
        }

        /// <summary>
        /// original text
        /// </summary>
        public string OriginalText { get; private set; }

        /// <summary>
        /// total count of scentence
        /// note: there might be some empty scentence.
        ///       e.g.: if the text contains "??", there will be an empty scentence;
        ///       cause the ? character is consider as a spliter of scentence
        /// </summary>
        public int ScentenceCount
        {
            get { return this.SentenceEndIndexes.Count; }
        }

        /// <summary>
        /// a list of words in the text
        /// </summary>
        public ICollection<string> Words
        {
            get { return this.WordStatistics.Keys; }
        }

        private ICollection<int> SentenceEndIndexes;

        private IDictionary<string, WordStatisticModel> WordStatistics;

        /// <summary>
        /// save each scentence ending index in the text by detecting the scentence spliters
        /// </summary>
        private void FillSentenceEnds()
        {
            this.SentenceEndIndexes = new List<int>();
            var match = regSentenceSpliter.Match(this.OriginalText);
            while (match != null && match.Success)
            {
                this.SentenceEndIndexes.Add(match.Index);
                match = match.NextMatch();
            }
            this.SentenceEndIndexes.Add(this.OriginalText.Length - 1);
        }

        /// <summary>
        /// call this method once, then you can call the get functions to get the generated concordance infomation
        /// </summary>
        public void GenerateConcordance()
        {
            this.WordStatistics = new Dictionary<string, WordStatisticModel>();

            for (var i = 0; i < this.SentenceEndIndexes.Count; i++)
            {
                var scentence = this.GetScentence(i);
                if (string.IsNullOrWhiteSpace(scentence)) continue;

                var match = regWordSpliter.Match(scentence);
                var wordStartIndex = 0;
                var wordEndIndex = 0;
                while (match != null && match.Success)
                {
                    wordEndIndex = match.Index;
                    this.WordOccursInScentence(scentence, wordStartIndex, wordEndIndex, i);
                    wordStartIndex = wordEndIndex;
                    match = match.NextMatch();
                }
                if (wordEndIndex < scentence.Length - 1)
                {
                    wordEndIndex = scentence.Length - 1;
                    this.WordOccursInScentence(scentence, wordStartIndex, wordEndIndex, i);
                }
            }
        }

        /// <summary>
        /// get the scentence at the given scentence index
        /// </summary>
        /// <param name="index">given scentence index</param>
        /// <returns>scentence text</returns>
        public string GetScentence(int index)
        {
            if (index < 0 || this.SentenceEndIndexes.Count <= index) return null;

            var startIndex = (index == 0) ? 0 : this.SentenceEndIndexes.ElementAt(index - 1) + 1;
            var scentence = this.OriginalText.Substring(startIndex, this.SentenceEndIndexes.ElementAt(index) - startIndex + 1);
            return scentence;
        }

        /// <summary>
        /// get the total occuring times of a given word
        /// </summary>
        /// <param name="word">given word</param>
        /// <returns>total occuring times</returns>
        public int GetWordOccurs(string word)
        {
            var model = GetWordStatistic(word);
            return model.Occurs;
        }

        /// <summary>
        /// get a list of indexes indicates a given word occurs in which scentences
        /// </summary>
        /// <param name="word">given word</param>
        /// <returns>scentence index list</returns>
        public ICollection<int> GetWordOccuredSentenceIndexes(string word)
        {
            var model = GetWordStatistic(word);
            return model.GetOccuredSentencIndexes();
        }

        /// <summary>
        /// get a list of indexes indicates a given word occurs in a given scentence
        /// </summary>
        /// <param name="word">given word</param>
        /// <param name="scentenceIndex">index of scentence</param>
        /// <returns>word occurs indexes in the scentence</returns>
        public ICollection<int> GetWordOccursInSentence(string word, int scentenceIndex)
        {
            var model = GetWordStatistic(word);
            return model.GetOccursInSentence(scentenceIndex);
        }

        /// <summary>
        /// get the statistic model of a given word
        /// </summary>
        /// <param name="word">given word</param>
        /// <returns>the statistic model</returns>
        private WordStatisticModel GetWordStatistic(string word)
        {
            var myWord = word.Trim().ToLower();
            if (string.IsNullOrEmpty(myWord) || !this.WordStatistics.Keys.Contains(myWord)) return new WordStatisticModel(myWord);
            return this.WordStatistics[myWord];
        }

        /// <summary>
        /// verify a given pair of start and end index in a given scentence
        /// if the string candidate satisfies the validator regular expression, add the occurance and itself in the statistic model
        /// </summary>
        /// <param name="scentence">given scentence text</param>
        /// <param name="wordStartIndex">start index of the word</param>
        /// <param name="wordEndIndex">end index of the word</param>
        /// <param name="sentenceIndex">given scentence index in the article</param>
        private void WordOccursInScentence(string scentence, int wordStartIndex, int wordEndIndex, int sentenceIndex)
        {
            if (wordEndIndex <= wordStartIndex) return;

            var word = scentence.Substring(wordStartIndex, (wordEndIndex - wordStartIndex));
            var match = regWordCharacter.Match(word);

            if (match == null || match.Length <= 0) return;

            word = word.Substring(match.Index, match.Length).ToLower();

            if (string.IsNullOrWhiteSpace(word)) return;

            if (this.WordStatistics.Keys.Contains(word))
            {
                var statistics = this.WordStatistics[word];
                statistics.AddOccuring(sentenceIndex, wordStartIndex);
            }
            else
            {
                var statistics = new WordStatisticModel(word);
                statistics.AddOccuring(sentenceIndex, wordStartIndex);
                this.WordStatistics.Add(word, statistics);
            }
        }
    }
}
