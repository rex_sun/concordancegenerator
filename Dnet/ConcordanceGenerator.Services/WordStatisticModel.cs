﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcordanceGenerator.Services
{
    /// <summary>
    /// internal class to save word statistic
    /// </summary>
    internal class WordStatisticModel
    {
        internal WordStatisticModel(string word)
        {
            this.Word = word;
            this.Occurs = 0;
        }
        
        /// <summary>
        /// count the word occurs
        /// </summary>
        internal int Occurs { get; private set; }

        /// <summary>
        /// word itself
        /// </summary>
        internal string Word { get; private set; }

        private IDictionary<int, List<int>> occursInSentence = new Dictionary<int, List<int>>();

        /// <summary>
        /// when a word occurs in a scentence, add the starting index
        /// </summary>
        /// <param name="sentenceIndex">scentence index</param>
        /// <param name="index">word starting index</param>
        internal void AddOccuring(int sentenceIndex, int index)
        {
            if (this.occursInSentence.Keys.Contains(sentenceIndex))
            {
                var occurs = this.occursInSentence[sentenceIndex];
                occurs.Add(index);
            }
            else
            {
                this.occursInSentence.Add(sentenceIndex, new List<int> { index });
            }

            this.Occurs++;
        }

        /// <summary>
        /// get a list of starting index of the word occurs in a given scentence
        /// </summary>
        /// <param name="sentenceIndex">scentence index</param>
        /// <returns>list of starting index</returns>
        internal ICollection<int> GetOccursInSentence(int sentenceIndex)
        {
            if (!this.occursInSentence.Keys.Contains(sentenceIndex)) return null;            
            return this.occursInSentence[sentenceIndex];
        }

        /// <summary>
        /// get a list of scentence index in which the word occurs
        /// </summary>
        /// <returns>list of scentence index</returns>
        internal ICollection<int> GetOccuredSentencIndexes()
        {
            return occursInSentence.Keys;
        }
    }
}
