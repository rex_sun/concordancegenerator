﻿using ConcordanceGenerator.Helper;
using System.IO;

namespace ConcordanceGenerator.WebAPI
{
    public class ConcordanceGeneratorService : IConcordanceGeneratorService
    {
        public ConcordanceResultModel FromUrl(string url)
        {
            var text = Helpers.GetWebResponse(url);
            var result = new ConcordanceResultModel(text);
            return result;
        }

        public ConcordanceResultModel FromFile(Stream fileStream)
        {
            using (var reader = new StreamReader(fileStream))
            {
                var text = reader.ReadToEnd();
                var result = new ConcordanceResultModel(text);
                return result;
            }
        }
    }
}
