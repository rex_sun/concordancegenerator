﻿using ConcordanceGenerator.Services;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ConcordanceGenerator.WebAPI
{
    [ServiceContract]
    public interface IConcordanceGeneratorService
    {
        [OperationContract, WebGet]
        ConcordanceResultModel FromUrl(string url);

        [OperationContract, WebInvoke(Method = "POST")]
        ConcordanceResultModel FromFile(Stream fileStream);
    }

    [DataContract]
    public class ConcordanceResultModel
    {
        public ConcordanceResultModel(string text)
            : this(new ConcordanceModel(text)) { }

        public ConcordanceResultModel(ConcordanceModel model)
        {
            this.WordsCount = 0;
            this.Words = new List<WordResultModel>();

            if (model == null) return;
            model.GenerateConcordance();

            var words = new List<string>();
            words.AddRange(model.Words);
            words.Sort((x, y) => { return string.Compare(x, y); });

            foreach (var word in words)
            {
                var occursInfo = new List<OccurResultModel>();
                var occuredSentenceIndexes = model.GetWordOccuredSentenceIndexes(word);

                foreach (var scentenceIndex in occuredSentenceIndexes)
                {
                    var occurInScentence = model.GetWordOccursInSentence(word, scentenceIndex);

                    foreach (var occurIndex in occurInScentence)
                    {
                        var occurResult = new OccurResultModel { ScentenceIndex = scentenceIndex, OccurIndex = occurIndex };
                        occursInfo.Add(occurResult);
                    }
                }

                var wordResult = new WordResultModel {
                    Text = word,
                    OccursCount = model.GetWordOccurs(word),
                    OccursInfo = occursInfo,
                };

                this.Words.Add(wordResult);
                this.WordsCount += wordResult.OccursCount;
            }

            if (this.WordsCount > 0)
            {
                foreach (var wordItem in this.Words)
                {
                    wordItem.OccursRate = ((float)wordItem.OccursCount / (float)this.WordsCount);
                }
            }
        }

        [DataMember]
        public List<WordResultModel> Words { get; private set; }

        [DataMember]
        public int WordsCount { get; private set; }
    }

    [DataContract]
    public class WordResultModel
    {
        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public int OccursCount { get; set; }

        [DataMember]
        public float OccursRate { get; set; } 

        [DataMember]
        public List<OccurResultModel> OccursInfo { get; set; }
    }

    [DataContract]
    public class OccurResultModel
    {
        [DataMember]
        public int ScentenceIndex { get; set; }

        [DataMember]
        public int OccurIndex { get; set; }
    }
}
