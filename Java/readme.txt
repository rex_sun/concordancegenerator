1. Make sure Apache Maven 3.1.1, JDK 7.0+, and Tomcat 7.0 installed.

2. To undeploy existing website, start a command console with admin permission from this folder:
mvn tomcat:undeploy

3. To clean, build, and deploy the website to Tomcat, start a command console with admin permission from this folder:
mvn clean package tomcat:deploy -e -X -P tomcat-localhost

4. After deploying it to be Tomcat
start the website at: http://localhost:8080/concordance-generator-webap
try the get service at: http://localhost:8080/concordance-generator-webapi/api/services/fromurl?url=http://www.gutenberg.org/cache/epub/1524/pg1524.txt
try the post service at: http://localhost:8080/concordance-generator-webapi/api/services/fromfile (with file content posted in stream, and the parameter named "file")