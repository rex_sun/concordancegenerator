package org.ConcordanceGenerator.test;

import org.ConcordanceGenerator.helper.Helpers;

public final class ConcordanceGeneratorSampleText {

    public final static String OneSentence_1 = "Hello world!";

    public final static String OneSentence_2 = "Hello world, we have a new world?";
    
    public final static String OneSentence_3 = "Hello world, we could have a better world if we want.";

    public final static String OneSentence_4 = "Hello world, saying: 'this is a better world -- than we can expect'.";

    public final static String OneSentence_5 = "Hello world, saying:&109 ^ &(% _+0` 09867 'this is a bet8*ter wOrld -- th@@#%$an we can expect'.";

    public final static String TwoSentences_1 = "Hello world! Hello the world!";

    public final static String TwoSentences_2 = "Hello world, we have a new world? I think we have!";

    public final static String TwoSentences_3 = "Hello world, we could have a better world if we want. Try to do it!";

    public final static String TwoSentences_4 = "Hello world, saying: 'this is a better world -- than we can expect'. Why not to expect a better world then?";

    public final static String TwoSentences_5 = "Hello world, saying:&109 ^ &(% _+0` 09867 'this is a bet8*ter wOrld -- th@@#%$an we can expect'. I can read what you are saying *(&5 1023!";
}
