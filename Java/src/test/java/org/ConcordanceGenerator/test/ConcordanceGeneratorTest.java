package org.ConcordanceGenerator.test;

import java.util.*;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.ConcordanceGenerator.services.ConcordanceModel;

public class ConcordanceGeneratorTest {
    protected static List<Integer> GenerateList(int ... arr){
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < arr.length; i++) {
            result.add(arr[i]);
        }
        return result;
    }
    
    protected static boolean ArrayEqual(List<Integer> expected, List<Integer> actural) {
        if (expected == null)
        {
            if (actural != null) return false;
            else return true;
        }

        if (actural == null)
        {
            if (expected != null) return false;
            else return true;
        }

        if (expected.size() != actural.size()) return false;

        boolean result = true;
        for (int i = 0; i < expected.size(); i++)
        {
            result = result && (expected.get(i) == actural.get(i));
            if (!result) return false;
        }

        return true;
    }
    
    protected static boolean ArrayEqual(List<Integer> expected, Set<Integer> actural) {
        if (expected == null)
        {
            if (actural != null) return false;
            else return true;
        }

        if (actural == null)
        {
            if (expected != null) return false;
            else return true;
        }

        if (expected.size() != actural.size()) return false;

        List<Integer> acturalList = new ArrayList<Integer>();
        acturalList.addAll(actural);

        return ArrayEqual(expected, acturalList);
    }
    
    @Test
    public void GenerateConcordance_OneSentence(){
        ConcordanceModel model = new ConcordanceModel(ConcordanceGeneratorSampleText.OneSentence_1);
        model.GenerateConcordance();
        int expectedOccurs = 1;
        int acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 0;
        acturalOccurs = model.GetWordOccurs("we");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("worl");
        assertEquals(expectedOccurs, acturalOccurs);
        List<Integer> expectedOccursIndex = GenerateList(5);
        List<Integer> acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList(0);
        Set<Integer> acturalOccurSetIndex = model.GetWordOccuredSentenceIndexes("world");
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccurSetIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.OneSentence_2);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 5, 26 );
        acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.OneSentence_3);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("we");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 12, 44 );
        acturalOccursIndex = model.GetWordOccursInSentence("we", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 5, 35 );
        acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.OneSentence_4);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 1;
        acturalOccurs = model.GetWordOccurs("saying");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("this");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("expect");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 5, 38 );
        acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 12 );
        acturalOccursIndex = model.GetWordOccursInSentence("saying", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 21 );
        acturalOccursIndex = model.GetWordOccursInSentence("this", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 59 );
        acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.OneSentence_5);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 1;
        acturalOccurs = model.GetWordOccurs("saying");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("this");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("expect");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 5, 61 );
        expectedOccurs = 0;
        acturalOccurs = model.GetWordOccurs("better");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 0;
        acturalOccurs = model.GetWordOccurs("'bet8*ter");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 12 );
        acturalOccursIndex = model.GetWordOccursInSentence("saying", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 42 );
        acturalOccursIndex = model.GetWordOccursInSentence("this", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 87 );
        acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 0 );
        acturalOccurSetIndex = model.GetWordOccuredSentenceIndexes("world");
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccurSetIndex));
    }

    @Test
    public void GenerateConcordance_TwoSentences()
    {
        ConcordanceModel model = new ConcordanceModel(ConcordanceGeneratorSampleText.TwoSentences_1);
        model.GenerateConcordance();
        int expectedOccurs = 2;
        int acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 0;
        acturalOccurs = model.GetWordOccurs("we");
        assertEquals(expectedOccurs, acturalOccurs);
        List<Integer> expectedOccursIndex = GenerateList( 5 );
        List<Integer> acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 10 );
        acturalOccursIndex = model.GetWordOccursInSentence("world", 1);

        expectedOccurs = 10;
        acturalOccurs = acturalOccursIndex.get(0);
        assertEquals(expectedOccurs, acturalOccurs);
        
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 0, 1 );
        Set<Integer> acturalOccurSetIndex = model.GetWordOccuredSentenceIndexes("world");
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccurSetIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.TwoSentences_2);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("have");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 15 );
        acturalOccursIndex = model.GetWordOccursInSentence("have", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 11 );
        acturalOccursIndex = model.GetWordOccursInSentence("have", 1);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.TwoSentences_3);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("we");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 1;
        acturalOccurs = model.GetWordOccurs("it");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 12, 44 );
        acturalOccursIndex = model.GetWordOccursInSentence("we", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 10 );
        acturalOccursIndex = model.GetWordOccursInSentence("it", 1);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.TwoSentences_4);
        model.GenerateConcordance();
        expectedOccurs = 3;
        acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("a");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("expect");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 5, 38 );
        acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 27 );
        acturalOccursIndex = model.GetWordOccursInSentence("world", 1);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 59 );
        acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 11 );
        acturalOccursIndex = model.GetWordOccursInSentence("expect", 1);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));

        model = new ConcordanceModel(ConcordanceGeneratorSampleText.TwoSentences_5);
        model.GenerateConcordance();
        expectedOccurs = 2;
        acturalOccurs = model.GetWordOccurs("world");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccurs = model.GetWordOccurs("saying");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccursIndex = GenerateList( 5, 61 );
        expectedOccurs = 0;
        acturalOccurs = model.GetWordOccurs("better");
        assertEquals(expectedOccurs, acturalOccurs);
        expectedOccurs = 0;
        acturalOccurs = model.GetWordOccurs("'bet8*ter");
        assertEquals(expectedOccurs, acturalOccurs);
        acturalOccursIndex = model.GetWordOccursInSentence("world", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 12 );
        acturalOccursIndex = model.GetWordOccursInSentence("saying", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 24 );
        acturalOccursIndex = model.GetWordOccursInSentence("saying", 1);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 87 );
        acturalOccursIndex = model.GetWordOccursInSentence("expect", 0);
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccursIndex));
        expectedOccursIndex = GenerateList( 0, 1 );
        acturalOccurSetIndex = model.GetWordOccuredSentenceIndexes("saying");
        assertTrue(ArrayEqual(expectedOccursIndex, acturalOccurSetIndex));
    }
}
