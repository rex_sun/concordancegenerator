package org.ConcordanceGenerator.helper;

import java.io.*;
import java.net.*;
import java.util.*;

public final class Helpers {
    private final static String REQUEST_AGENT_TYPE = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)";
    private final static String REQUEST_CONNECTION_TYPE = "Keep-Alive";
    private final static String REQUEST_CONTENT_TYPE = "text/html; charset=utf-8";

    public static String GetWebResponse(String targetURL)
    {
        try {
            URL objUrl = new URL(targetURL);
            HttpURLConnection con = null;
            try {
                con = (HttpURLConnection) objUrl.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("User-Agent", REQUEST_AGENT_TYPE);
                con.setRequestProperty("Connection", REQUEST_CONNECTION_TYPE);
                con.setRequestProperty("Content-Type", REQUEST_CONTENT_TYPE);

                int responseCode = con.getResponseCode();
                return GetInputStreamText(con.getInputStream());
            } catch (Exception ex) {
                return null;
            } finally {
                if(con != null) {
                    con.disconnect(); 
                }
            }
        } catch (MalformedURLException exUrl) {
            return null;
        }
    }
    
    public static String GetInputStreamText(InputStream inStream) {
        BufferedReader in = new BufferedReader(new InputStreamReader(inStream));
        String inputLine;
        StringBuffer buffer = new StringBuffer();
        try {
            while ((inputLine = in.readLine()) != null) {
                buffer.append(inputLine);
            }
        } catch (IOException ex) {
            buffer = new StringBuffer();
        }
        return buffer.toString();
    }
}
