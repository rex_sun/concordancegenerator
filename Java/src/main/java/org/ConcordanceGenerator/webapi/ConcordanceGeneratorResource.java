package org.ConcordanceGenerator.webapi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Consumes;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import org.ConcordanceGenerator.services.ConcordanceModel;
import org.ConcordanceGenerator.services.ConcordanceResultModel;
import org.ConcordanceGenerator.helper.Helpers;

@Path("services")
public class ConcordanceGeneratorResource {

    @GET
    @Path("fromurl")
    @Produces(MediaType.APPLICATION_JSON)
    public ConcordanceResultModel FromUrl(@QueryParam("url") String url) {
        String text = Helpers.GetWebResponse(url);
        ConcordanceResultModel result = new ConcordanceResultModel(text);
        return result;
    }

    /*
    @GET
    @Path("fromtext")
    @Produces(MediaType.APPLICATION_JSON)
    public ConcordanceResultModel FromText(@QueryParam("text") String text) {
        ConcordanceResultModel result = new ConcordanceResultModel(text);
        return result;
    }
    */

    @POST
    @Path("fromfile")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ConcordanceResultModel FromFile(@FormDataParam("file") InputStream fileInputStream) {
        String text = Helpers.GetInputStreamText(fileInputStream);
        ConcordanceResultModel result = new ConcordanceResultModel(text);
        return result;
    }
}