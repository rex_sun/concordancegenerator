package org.ConcordanceGenerator.webapi;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.jettison.JettisonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import org.ConcordanceGenerator.services.JaxbContextResolver;

@ApplicationPath("api")
public class ConcordanceGeneratorApp extends ResourceConfig {
    public ConcordanceGeneratorApp() {
        super(ConcordanceGeneratorResource.class);
        this.packages("org.ConcordanceGenerator.services")
            .register(JaxbContextResolver.class)
            .register(JettisonFeature.class)
            .packages("org.glassfish.jersey.examples.multipart")
            .register(MultiPartFeature.class);
    }
}
