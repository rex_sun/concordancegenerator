package org.ConcordanceGenerator.services;

import java.util.*;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WordResultModel {
    public WordResultModel() {}
    public String Text;
    public int OccursCount;
    public float OccursRate;
    public List<OccurResultModel> OccursInfo;
}
