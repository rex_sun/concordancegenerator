package org.ConcordanceGenerator.services;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
//import java.util.Dictionary;
//import java.util.Enumeration;
import java.util.*;

public class ConcordanceModel {
    private static final Pattern patternSentenceSpliter = Pattern.compile("[.?!]", Pattern.MULTILINE);
    private static final Pattern patternWordSpliter = Pattern.compile("[^a-zA-Z]", Pattern.MULTILINE);
    private static final Pattern patternWordCharacter = Pattern.compile("[a-zA-Z]+", Pattern.MULTILINE);

    public ConcordanceModel(String text) {
        OriginalText = text;
        FillSentenceEnds();
    }
    
    public String OriginalText;
    
    public int GetScentenceCount() {
        return SentenceEndIndexes.size();
    }
    public Set<String> GetWords() {
        return WordStatistics.keySet();
    }
    
    private List<Integer> SentenceEndIndexes;

    private Map<String, WordStatisticModel> WordStatistics;

    private void FillSentenceEnds()
    {
        SentenceEndIndexes = new ArrayList<Integer>();
        Matcher matcher = patternSentenceSpliter.matcher(OriginalText);
        int endIndex = 0;
        while (matcher.find())
        {
            endIndex = matcher.end();
            SentenceEndIndexes.add(endIndex);
        }
        if (OriginalText.length() > endIndex) {
            SentenceEndIndexes.add(OriginalText.length());
        }
    }
    
    public void GenerateConcordance()
    {
        WordStatistics = new HashMap<String, WordStatisticModel>();

        for (int i = 0; i < SentenceEndIndexes.size(); i++)
        {
            String scentence = GetScentence(i);
            if (scentence == null || scentence.length() <= 0) continue;

            Matcher matcher = patternWordSpliter.matcher(scentence);
            int wordEndIndex = 0;
            int wordStartIndex = 0;
            while (matcher.find())
            {
                wordEndIndex = matcher.start();
                WordOccursInScentence(scentence, wordStartIndex, wordEndIndex, i);
                wordStartIndex = wordEndIndex;
            }
            if (wordEndIndex < scentence.length() - 1)
            {
                wordEndIndex = scentence.length() - 1;
                WordOccursInScentence(scentence, wordStartIndex, wordEndIndex, i);
            }
        }
    }

    public String GetScentence(int index)
    {
        if (index < 0 || SentenceEndIndexes.size() <= index) return null;

        int startIndex = (index == 0) ? 0 : SentenceEndIndexes.get(index - 1);
        String scentence = OriginalText.substring(startIndex, SentenceEndIndexes.get(index));
        return scentence;
    }
    
    public int GetWordOccurs(String word)
    {
        WordStatisticModel model = GetWordStatistic(word);
        return model.Occurs;
    }
    
    public Set<Integer> GetWordOccuredSentenceIndexes(String word)
    {
        WordStatisticModel model = GetWordStatistic(word);
        return model.GetOccuredSentencIndexes();
    }

    public List<Integer> GetWordOccursInSentence(String word, int scentenceIndex)
    {
        WordStatisticModel model = GetWordStatistic(word);
        return model.GetOccursInSentence(scentenceIndex);
    }
    
    private WordStatisticModel GetWordStatistic(String word)
    {
        String lowerWord = word.trim().toLowerCase();
        if (!WordStatistics.keySet().contains(lowerWord)) return new WordStatisticModel(lowerWord);
        return WordStatistics.get(lowerWord);
    }

    private void WordOccursInScentence(String scentence, int wordStartIndex, int wordEndIndex, int sentenceIndex)
    {
        if (wordEndIndex <= wordStartIndex) return;

        String word = scentence.substring(wordStartIndex, wordEndIndex);
        Matcher matcher = patternWordCharacter.matcher(word);

        if (!matcher.find()) return;

        word = word.substring(matcher.start(), matcher.end()).toLowerCase();

        if (word == null || word.length() <= 0) return;

        if (WordStatistics.keySet().contains(word))
        {
            WordStatisticModel statistics = WordStatistics.get(word);
            statistics.AddOccuring(sentenceIndex, wordStartIndex);
        }
        else
        {
            WordStatisticModel statistics = new WordStatisticModel(word);
            statistics.AddOccuring(sentenceIndex, wordStartIndex);
            WordStatistics.put(word, statistics);
        }
    }
}
