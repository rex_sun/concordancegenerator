package org.ConcordanceGenerator.services;

import java.util.*;

class WordStatisticModel {
    WordStatisticModel(String word) {
        Word = word;
        Occurs = 0;
        occursInSentence = new HashMap<Integer, List<Integer>>();
    }
    
    public int Occurs;
    public String Word;
    private Map<Integer, List<Integer>> occursInSentence;
    
    void AddOccuring(int sentenceIndex, int index) {
        if (occursInSentence.keySet().contains(sentenceIndex))
        {
            List<Integer> occurs = occursInSentence.get(sentenceIndex);
            occurs.add(index);
        }
        else
        {
            List<Integer> occurList = new ArrayList<Integer>();
            occurList.add(index);
            occursInSentence.put(sentenceIndex, occurList);
        }

        Occurs++;
    }
    
    List<Integer> GetOccursInSentence(int sentenceIndex) {
        if (!occursInSentence.keySet().contains(sentenceIndex)) return null;
        return occursInSentence.get(sentenceIndex);
    }
    
    Set<Integer> GetOccuredSentencIndexes() {
        return occursInSentence.keySet();
    }
}
