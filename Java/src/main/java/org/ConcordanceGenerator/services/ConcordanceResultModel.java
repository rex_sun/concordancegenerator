package org.ConcordanceGenerator.services;

import java.util.*;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConcordanceResultModel {
    public ConcordanceResultModel () {}
    
    public ConcordanceResultModel(String text) {
        this(new ConcordanceModel(text));
    }
    
    public ConcordanceResultModel(ConcordanceModel model) {
        WordsCount = 0;
        Words = new ArrayList<WordResultModel>();

        if (model == null) return;
        model.GenerateConcordance();

        List<String> words = new ArrayList<String>();
        words.addAll(model.GetWords());
        Collections.sort(words);

        for (String word : words)
        {
            List<OccurResultModel> occursInfo = new ArrayList<OccurResultModel>();
            Set<Integer> occuredSentenceIndexes = model.GetWordOccuredSentenceIndexes(word);

            for (int scentenceIndex : occuredSentenceIndexes)
            {
                List<Integer> occurInScentence = model.GetWordOccursInSentence(word, scentenceIndex);

                for (int occurIndex : occurInScentence)
                {
                    OccurResultModel occurResult = new OccurResultModel();
                    occurResult.ScentenceIndex = scentenceIndex;
                    occurResult.OccurIndex = occurIndex;
                    occursInfo.add(occurResult);
                }
            }

            WordResultModel wordResult = new WordResultModel();
            wordResult.Text = word;
            wordResult.OccursCount = model.GetWordOccurs(word);
            wordResult.OccursInfo = occursInfo;

            Words.add(wordResult);
            WordsCount += wordResult.OccursCount;
        }

        if (WordsCount > 0)
        {
            for (WordResultModel wordItem : Words)
            {
                wordItem.OccursRate = ((float)wordItem.OccursCount / (float)this.WordsCount);
            }
        }
    }
    
    public List<WordResultModel> Words;
    public int WordsCount;
    
}
