package org.ConcordanceGenerator.services;

import java.util.*;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OccurResultModel {
    public OccurResultModel() {}
    public int ScentenceIndex;
    public int OccurIndex;
}
